import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ResetPasswordService {

  private httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

  constructor(
    private http: HttpClient,
    private nzMessage: NzMessageService,) { }

  resetPassword = async (email: String): Promise<any> => {

    let result: any;
    try {
      result = await this.http.post("http://localhost:8001/api/user/reset_password?email=" + email, this.httpOptions)
        .toPromise();
      console.log(result);
    } catch (e) {
      console.log(e);
      result = e.error;
    }
    return result;
  }


}
