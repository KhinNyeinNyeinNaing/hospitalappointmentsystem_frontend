import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const AUTH_API = 'http://localhost:8001/api/user/';



@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

  constructor(private http: HttpClient) { }

  // login(username: string, password: string): Observable<any> {
  //   return this.http.post(AUTH_API + 'login?email='+username+'&password='+password, this.httpOptions);
  // }
  
  async login(username: string, password: string): Promise<any> {
    let result: any;
    try {
      result = await this.http.post(AUTH_API + 'login?email='+username+'&password='+password, this.httpOptions)
        .toPromise();
      console.log(result);
    } catch (e) {
      console.log(e);
      result = e.error;
    }
    return result;
    // return this.http.post(AUTH_API + 'login?email='+username+'&password='+password, httpOptions);
  }

  register(username: string, email: string, password: string): Observable<any> {
    return this.http.post(AUTH_API + 'signup', {
      username,
      email,
      password
    }, this.httpOptions);
  }
}