import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { TokenStorageService } from '../../services/token-storage.service';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  validateForm!: FormGroup;
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = []; 

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService, 
    private tokenStorage: TokenStorageService,
    private messageService: NzMessageService) {}

  ngOnInit(): void {
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
    }
    this.validateForm = this.fb.group({
      userName: [null, [Validators.required,Validators.email]],
      password: [null, [Validators.required]],
      remember: [true]
    });
  }
  submitForm = async () => {
    // validation
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
    if (!this.validateForm.invalid) {
      let result = await this.authService.login(
        this.validateForm.controls.userName.value,
        this.validateForm.controls.password.value
      );
      if (result.success) 
      {
        this.tokenStorage.saveToken(result.data.accessToken);
          this.tokenStorage.saveUser(result.data);
  
          this.isLoginFailed = false;
          this.isLoggedIn = true;
          this.messageService.success('Logged in successfully.');
          this.router.navigateByUrl('/menu');
      }
      } else {
        this.messageService.error(
          'Failed to log in!'
        );
      }
    }
    

}
